creae or alter procedure sp_PUD_ETL_data_persona_discapacitados
as
	select 
		case 
			when e.IdTipoDocIdentidad =  1 then '01'
			when e.IdTipoDocIdentidad =  2 then '04'
			when e.IdTipoDocIdentidad =  3 then '00'
			when e.IdTipoDocIdentidad =  4 then '11'
			when e.IdTipoDocIdentidad =  5 then '07'
			when e.IdTipoDocIdentidad =  6 then '00'
		end as TipoDocIdentidad, c.NumDocumento, c.Nombres, c.ApPaterno, c.ApMaterno, c.Genero as Sexo, cast(c.Fecha_Nacimiento as date) as Fecha_Nacimiento, 
		cast(c.FeFallecimiento as date) FeFallecimiento
	from TB_CERTIFICADO a
	inner join TB_Registro b on b.IdRegistro = a.IdRegistro
	inner join TB_Persona_Natural c on c.IdPersonaNatural = b.IdPersonaNatural
	inner join TB_Solicitud d on d.IdSolicitud = b.IdSolicitudCurrent
	inner join TB_TIPO_DOC_IDENTIDAD e on e.IdTipoDocIdentidad = c.IdTipoDocumento
	inner join sirnpd.dbo.TB_RESOLUCION RE (nolock) on RE.IdRegistro = a.IdRegistro and RE.Estado = 1   
	where a.Estado = 1 and b.FeInscripcion is not null
	and LimConducta is not null and b.Inf_Status = 1
	and RE.IdTipoResolucion not in (4,5) and c.EstadoReniec = 1

 -- exec sp_PUD_ETL_data_persona_discapacitados