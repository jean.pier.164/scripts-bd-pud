create or alter procedure sp_PUD_get_personas_sexo_rangoedad_x_ubigeo_edades(
	@UbigeoDomicilio varchar(8),
	@EdadMin int,
	@EdadMax int
)
as

DECLARE @Table1 AS TABLE 
(
	UbigeoDomicilio varchar(8),
	Sexo char(1),
	edad int
)

DECLARE @Table2 AS TABLE 
(
	UbigeoDomicilio varchar(8),
	Sexo char(1),
	Edad int,
	RangoEdad varchar(8),
	RangoEdadOrder int
)

-- Data de instituciones (UNION)
 insert into @Table1
	select b.UbigeoDomicilio, b.Sexo,
	floor(
	(cast(convert(varchar(8),getdate(),112) as int)-
	cast(convert(varchar(8),b.FecNacimiento,112) as int) ) / 10000
	) as edad
	from TB_CONADIS_DISCAPACITADOS a
	inner join TB_PERSONA b on b.NumeroDocIdentidad = a.NumDocumentoIde and b.CodTipoDoc = a.CodTipoDocumentoIde



insert into @Table2
select * ,
case 
	 when edad >= 0 and edad <= 5 then '0 - 5'
	 when edad >= 6 and edad <= 10 then '6 - 10'
	 when edad >= 11 and edad <= 15 then '11 - 15'
	 when edad >= 16 and edad <= 20 then '16 - 20'
	 when edad >= 21 and edad <= 25 then '21 - 25'
	 when edad >= 26 and edad <= 30 then '26 - 30'
	 when edad >= 31 and edad <= 35 then '31 - 35'
	 when edad >= 36 and edad <= 40 then '36 - 40'
	 when edad >= 41 and edad <= 45 then '41 - 45'
	 when edad >= 46 and edad <= 50 then '46 - 50'
	 when edad >= 51 and edad <= 55 then '51 - 55'
	 when edad >= 56 and edad <= 60 then '56 - 60'
	 when edad >= 61 and edad <= 64 then '61 - 65'
	 when edad >= 65 then '+65'
end rangoedades,
case 
	 when edad >= 0 and edad <= 5 then 1
	 when edad >= 6 and edad <= 10 then 2
	 when edad >= 11 and edad <= 15 then 3
	 when edad >= 16 and edad <= 20 then 4
	 when edad >= 21 and edad <= 25 then 5
	 when edad >= 26 and edad <= 30 then 6
	 when edad >= 31 and edad <= 35 then 7
	 when edad >= 36 and edad <= 40 then 8
	 when edad >= 41 and edad <= 45 then 9
	 when edad >= 46 and edad <= 50 then 10
	 when edad >= 51 and edad <= 55 then 11
	 when edad >= 56 and edad <= 60 then 12
	 when edad >= 61 and edad <= 64 then 13
	 when edad >= 65 then 14
end rangoedadesorder
from @Table1
where edad between @EdadMin and @EdadMax

select count(Sexo) as Cantidad, Sexo, RangoEdad, RangoEdadOrder
from @Table2
group by Sexo, RangoEdad, RangoEdadOrder
order by RangoEdadOrder, Sexo