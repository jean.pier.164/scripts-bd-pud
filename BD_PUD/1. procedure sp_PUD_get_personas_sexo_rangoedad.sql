create or alter procedure sp_PUD_get_personas_sexo_rangoedad(
	@UbigeoDomicilio varchar(8)
)
as

DECLARE @Table1 AS TABLE 
(
	UbigeoDomicilio varchar(8),
	Sexo char(1),
	edad int
)

DECLARE @Table2 AS TABLE 
(
	UbigeoDomicilio varchar(8),
	Sexo char(1),
	Edad int,
	RangoEdad varchar(8),
	RangoEdadOrder int
)

-- Data de instituciones (UNION)
 insert into @Table1
	select b.UbigeoDomicilio, b.Sexo,
	floor(
	(cast(convert(varchar(8),getdate(),112) as int)-
	cast(convert(varchar(8),b.FecNacimiento,112) as int) ) / 10000
	) as edad
	from TB_CONADIS_DISCAPACITADOS a
	inner join TB_PERSONA b on b.NumeroDocIdentidad = a.NumDocumentoIde and b.CodTipoDoc = a.CodTipoDocumentoIde



insert into @Table2
select * ,
case 
	 when edad <= 17 then '-18'
	 when edad >= 18 and edad <= 24 then '18 - 24'
	 when edad >= 25 and edad <= 34 then '25 - 34'
	 when edad >= 35 and edad <= 44 then '35 - 44'
	 when edad >= 45 and edad <= 54 then '45 - 54'
	 when edad >= 55 and edad <= 64 then '55 - 64'
	 when edad >= 65 then '+65'
end rangoedades,
case 
	 when edad <= 17 then 1
	 when edad >= 18 and edad <= 24 then 2
	 when edad >= 25 and edad <= 34 then 3
	 when edad >= 35 and edad <= 44 then 4
	 when edad >= 45 and edad <= 54 then 5
	 when edad >= 55 and edad <= 64 then 6
	 when edad >= 65 then 7
end rangoedadesorder
from @Table1

select count(Sexo) as Cantidad, Sexo, RangoEdad, RangoEdadOrder
from @Table2
group by Sexo, RangoEdad, RangoEdadOrder
order by RangoEdadOrder, Sexo