create or alter FUNCTION [dbo].[FN_GetGravedadNivel] (@lim1 int ,@lim2 int,@lim3 int,@lim4 int,@lim5 int,@lim6 int,@lim7 int )  
returns varchar(16)  
begin    
 declare @value int = 9  
 set @value =    
 CASE   
 WHEN (@lim1 BETWEEN 4 AND 6) OR (@lim2 BETWEEN 4 AND 6) OR (@lim3 BETWEEN 4 AND 6) OR (@lim4 BETWEEN 4 AND 6) OR (@lim5 BETWEEN 4 AND 6) OR (@lim6 BETWEEN 4 AND 6) OR (@lim7 BETWEEN 4 AND 6) THEN 3  
 WHEN (@lim1 BETWEEN 2 AND 3) OR (@lim2 BETWEEN 2 AND 3) OR (@lim3 BETWEEN 2 AND 3) OR (@lim4 BETWEEN 2 AND 3) OR (@lim5 BETWEEN 2 AND 3) OR (@lim6 BETWEEN 2 AND 3) OR (@lim7 BETWEEN 2 AND 3) THEN 2  
 WHEN (@lim1 = 1)       OR (@lim2 = 1)       OR (@lim3 = 1)         OR (@lim4 = 1)     OR (@lim5 = 1)         OR (@lim6 = 1)    OR (@lim7 = 1) THEN 1  
 ELSE 9  
 END   
 return @value  
end