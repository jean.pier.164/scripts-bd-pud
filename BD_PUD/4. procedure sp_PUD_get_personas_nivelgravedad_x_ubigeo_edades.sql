create or alter procedure sp_PUD_get_personas_nivelgravedad_x_ubigeo_edades(
	@UbigeoDomicilio varchar(8),
	@EdadMin int,
	@EdadMax int,
	@sexo char(1)
)
as

DECLARE @Table1 AS TABLE 
(
	UbigeoDomicilio varchar(8),
	Sexo char(1),
	edad int,
	LimConducta int,
	LimComunicacion int,
	LimCuidado int,
	LimLocomocion int,
	LimCorporal int,
	LimDestreza int,
	LimSituacion int
)

DECLARE @Table2 AS TABLE 
(
	UbigeoDomicilio varchar(8),
	Sexo char(1),
	Edad int,
	NivelGravedad varchar(20)
)

-- Data de instituciones (UNION)
 insert into @Table1
	select b.UbigeoDomicilio, b.Sexo,
	floor(
	(cast(convert(varchar(8),getdate(),112) as int)-
	cast(convert(varchar(8),b.FecNacimiento,112) as int) ) / 10000
	) as edad,
	a.LimConducta, a.LimComunicacion, a.LimCuidado, a.LimLocomocion, a.LimCorporal, a.LimDestreza, a.LimSituacion
	from TB_CONADIS_DISCAPACITADOS a
	inner join TB_PERSONA b on b.NumeroDocIdentidad = a.NumDocumentoIde and b.CodTipoDoc = a.CodTipoDocumentoIde
	

insert into @Table2
select UbigeoDomicilio, Sexo, edad,
	case when dbo.FN_GetGravedadNivel(LimConducta,LimComunicacion,LimCuidado, 
				LimLocomocion,LimCorporal,LimDestreza,LimSituacion) = 1 then 'Leve'
		 when dbo.FN_GetGravedadNivel(LimConducta,LimComunicacion,LimCuidado, 
				LimLocomocion,LimCorporal,LimDestreza,LimSituacion) = 2 then 'Moderado'
	     when dbo.FN_GetGravedadNivel(LimConducta,LimComunicacion,LimCuidado, 
				LimLocomocion,LimCorporal,LimDestreza,LimSituacion) = 3 then 'Severo'
		 when dbo.FN_GetGravedadNivel(LimConducta,LimComunicacion,LimCuidado, 
				LimLocomocion,LimCorporal,LimDestreza,LimSituacion) = 9 then 'Sin especificar'
	end
from @Table1
where edad between @EdadMin and @EdadMax
		and (@sexo = '' or Sexo = @sexo)

select count(Sexo) as Cantidad, Sexo, NivelGravedad
from @Table2
group by Sexo, NivelGravedad
order by NivelGravedad, Sexo